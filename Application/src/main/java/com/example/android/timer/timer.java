package com.example.android.timer;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.android.bluetoothlegatt.R;

public class timer extends Activity {
    private Long startTime;
    private Handler handler = new Handler();
    private Button startButton;
    private Button stopButton;
    private Button clearButton;
    private Button saveButton;
    private Long spentTime = (long) 0;
    private boolean lock = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timer);
        //取得目前時間
        startTime = System.currentTimeMillis();

        startButton = (Button) findViewById(R.id.start);
        stopButton = (Button) findViewById(R.id.stop);
        clearButton = (Button) findViewById(R.id.clear);
        saveButton = (Button) findViewById(R.id.save);
        startButton.setOnClickListener(new StartButtonListener());
        stopButton.setOnClickListener(new StopButtonListener());
        clearButton.setOnClickListener(new ClearButtonListener());
        saveButton.setOnClickListener(new SaveButtonListener());
        lock = false;
    }

    //固定要執行的方法
    private Runnable updateTimer = new Runnable() {
        public void run() {
            final TextView time = (TextView) findViewById(R.id.timer);
            //Long spentTime = System.currentTimeMillis() - startTime;
            //計算目前已過分鐘數
            spentTime = spentTime + 100;
            Long minius = (spentTime / 1000) / 60;
            //計算目前已過秒數
            Long seconds = (spentTime / 1000) % 60;
            Long minisecond = (spentTime / 100) % 10;
            time.setText(minius + ":" + seconds + ":" + minisecond);
            handler.postDelayed(this, 100);
        }
    };

    class StartButtonListener implements View.OnClickListener {

        public void onClick(View v) {
            if (lock != true) {
                handler.post(updateTimer);
                lock = true;
            }
        }

    }

    class StopButtonListener implements View.OnClickListener {

        public void onClick(View v) {
            if (lock) {
                handler.removeCallbacks(updateTimer);
                lock = false;
            }
        }

    }

    class ClearButtonListener implements View.OnClickListener {

        public void onClick(View v) {
            //handler.removeMessages();
            spentTime = (long) 0;
            final TextView time = (TextView) findViewById(R.id.timer);
            time.setText(0 + ":" + 0 + ":" + 0);
        }

    }

    class SaveButtonListener implements View.OnClickListener {

        public void onClick(View v) {
            //handler.removeMessages();

        }

    }
}
