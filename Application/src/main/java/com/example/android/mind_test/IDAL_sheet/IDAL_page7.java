package com.example.android.mind_test.IDAL_sheet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;
import com.example.android.mind_test.Mind_test;

public class IDAL_page7 extends AppCompatActivity {

    public static final String Q3_ANSWER_KEY = "Q4";

    //設定參數,radio=4
    private TextView m_tv_no;
    private TextView m_tv_question;
    private RadioButton m_radio_a;
    private RadioButton m_radio_b;
    private RadioButton m_radio_c;
    private RadioButton m_radio_d;



    public String lastscore,score;
    public String ScoreValue;
    public boolean StopAlarm=false;

    private CharSequence m_answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idal_page7_layout);

        //get last score value
        Bundle bundle=getIntent().getExtras();
        lastscore=bundle.getString("score6");


        //hint text
        ScoreValue=lastscore;
        Toast toast = Toast.makeText(this, ScoreValue, Toast.LENGTH_SHORT);
        toast.show();

        init();
    }
    //返回時score重新計算
    @Override
    protected  void onResume()
    {
        super.onPause();
        score=lastscore;
    }


    //起始畫面，設定起始畫面的按鈕,radio=4
    private void init() {
        m_tv_no = (TextView)findViewById(R.id.idal_tv_no);
        m_tv_question = (TextView)findViewById(R.id.idal_question_tv);
        m_radio_a = (RadioButton)findViewById(R.id.idal_radio_A_04);
        m_radio_b = (RadioButton)findViewById(R.id.idal_radio_B_04);
        m_radio_c = (RadioButton)findViewById(R.id.idal_radio_C_04);
        m_radio_d = (RadioButton)findViewById(R.id.idal_radio_D_04);





//題號
        m_tv_no.setText("7.");
//第一題的問題
        m_tv_question.setText(Html.fromHtml(getString(R.string.idal_07)));
//radio第一個選擇
        m_radio_a.setText(Html.fromHtml(getString(R.string.idal_ans_7A)));
//radio第二個選擇
        m_radio_b.setText(Html.fromHtml(getString(R.string.idal_ans_7B)));
        m_radio_c.setText(Html.fromHtml(getString(R.string.idal_ans_7C)));
        m_radio_d.setText(Html.fromHtml(getString(R.string.idal_ans_7D)));

    }

    // 按下 MAIN
    public void next(View view) {
        Intent intent = new Intent(this,IDAL_page8.class);
        getScore();
        if(StopAlarm)
        {
            this.onResume();
        }
        else {
            Bundle bundle = new Bundle();
            bundle.putString("score7", score);

            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    // 按下 BACK
    public void back(View view) {
        finish();
    }

    //radio=4
    public void getScore()
    {

        if(m_radio_a.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_b.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_c.isChecked())
        {
            score+="7.服用藥物"+"\n";
            StopAlarm=false;
        }
        else if(m_radio_d.isChecked())
        {
            score+="7.服用藥物"+"\n";
            StopAlarm=false;
        }

        else
        {
            //hint text
            StopAlarm=true;
            String alarm="請選擇一個選項喔!";
            Toast toast = Toast.makeText(this, alarm, Toast.LENGTH_SHORT);
            toast.show();

        }

    }


    //radio_on_click
    public void radio_a_onclick04(View view)
    {
        m_radio_a.isChecked();
    }

    public void radio_b_onclick04(View view)
    {
        m_radio_b.isChecked();
    }

    public void radio_c_onclick04(View view)
    {
        m_radio_c.isChecked();
    }

    public void radio_d_onclick04(View view)
    {
        m_radio_d.isChecked();
    }


    //radio_on_click

}
