package com.example.android.mind_test.Question_Sheet;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;

public class Question_page1 extends AppCompatActivity {

    public static final String Q1_ANSWER_KEY = "Q1";


    //設定參數
    private TextView m_tv_no;
    private TextView m_tv_question;
    private RadioButton m_radio_a;
    private RadioButton m_radio_b;
    private RadioButton m_radio_c;
    private RadioButton m_radio_d;
    private RadioButton m_radio_e;

    public int score;
    public boolean StopAlarm=false;


    private CharSequence m_answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_page1_layout);
        init();
    }

    //返回時score重新計算
    @Override
    protected  void onResume()
    {
        super.onResume();
        score=0;
    }

    //起始畫面，設定起始畫面的按鈕
    private void init() {
        m_tv_no = (TextView)findViewById(R.id.tv_no);
        m_tv_question = (TextView)findViewById(R.id.tv_question);
        m_radio_a = (RadioButton)findViewById(R.id.radio_a);
        m_radio_b = (RadioButton)findViewById(R.id.radio_b);
        m_radio_c = (RadioButton)findViewById(R.id.radio_c);
        m_radio_d=(RadioButton)findViewById(R.id.radio_d);
        m_radio_e=(RadioButton)findViewById(R.id.radio_e);



//題號
        m_tv_no.setText("1");
//第一題的問題
        m_tv_question.setText(Html.fromHtml(getString(R.string.question_1)));
//radio第一個選擇
        m_radio_a.setText(Html.fromHtml(getString(R.string.question_1_radio_a)));
//radio第二個選擇
        m_radio_b.setText(Html.fromHtml(getString(R.string.question_1_radio_b)));
//radio第三個選擇
        m_radio_c.setText(Html.fromHtml(getString(R.string.question_1_radio_c)));
//radio第四個選擇
        m_radio_d.setText(Html.fromHtml(getString(R.string.question_1_radio_d)));
//radio第五個選擇
        m_radio_e.setText(Html.fromHtml(getString(R.string.question_1_radio_e)));
    }
    //返回上一頁
    public void back(View view) {
        finish();
    }
    // 按下 NEXT
    public void next(View view) {


        // 建立新 Intent: new Intent( 來源 , 目的);
        getScore();
        if(StopAlarm)
        {
            this.onResume();
        }
        else {
            Intent intent = new Intent(this, Question_page2.class);
            Bundle bundle = new Bundle();
            bundle.putInt("score1", score);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }


    public void getScore()
    {

        if(m_radio_a.isChecked())
        {
            score+=0;
            StopAlarm=false;
        }
        else if(m_radio_b.isChecked())
        {
            score+=1;
            StopAlarm=false;
        }
        else if(m_radio_c.isChecked())
        {
            score+=2;
            StopAlarm=false;
        }
        else if(m_radio_d.isChecked())
        {
            score+=3;
            StopAlarm=false;
        }
        else if(m_radio_e.isChecked())
        {
            score+=4;
            StopAlarm=false;
        }
        else
        {
            //hint text
            StopAlarm=true;
            String alarm="請選擇一個選項喔!";
            Toast toast = Toast.makeText(this, alarm, Toast.LENGTH_SHORT);
            toast.show();

        }

    }



    public void radio_a_onclick(View view)
    {
        m_radio_a.isChecked();
    }

    public void radio_b_onclick(View view)
    {
        m_radio_b.isChecked();
    }

    public void radio_c_onclick(View view)
    {
        m_radio_c.isChecked();
    }
    public void radio_d_onclick(View view)
    {
        m_radio_d.isChecked();
    }
    public void radio_e_onclick(View view)
    {
        m_radio_e.isChecked();
    }



}