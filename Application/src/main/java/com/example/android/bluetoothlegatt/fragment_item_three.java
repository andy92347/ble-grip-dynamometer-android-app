package com.example.android.bluetoothlegatt;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class fragment_item_three extends Fragment {
    private MyAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        //return inflater.inflate(R.layout.fragment_item_three, null);
        View view = inflater.inflate(R.layout.fragment_item_three, null);
        ArrayList<String> myDataset = new ArrayList<>();
        /* set how many list */
        for (int i = 0; i < 2; i++) {
            myDataset.add(Integer.toString(i));
        }
        mAdapter = new MyAdapter(myDataset);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private List<String> mData;

        /* different view(text,image) is set in here */
        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTextView;
            public ImageView mImageView;

            public ViewHolder(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.info_text);
                mImageView = (ImageView) v.findViewById(R.id.info_img);
            }
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public MyAdapter(List<String> data) {
            mData = data;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_item_three_list, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            /* different view(text,image) is set in here */
            switch (mData.get(position)) {
                case "0":
                    holder.mTextView.setText("Calculate By Day");
                    holder.mImageView.setImageResource(R.drawable.icons8_calendar_1);


                    break;
                case "1":
                    holder.mTextView.setText("Calculate By Week");
                    holder.mImageView.setImageResource(R.drawable.icons8_calendar_7);
                    //holder.itemView.setBackgroundResource(R.drawable.buttonshape1);
                    //holder.itemView.setBackgroundColor(getResources().getColor(R.color.holo_light_primary,null));
                    break;
                case "2":
                    holder.mTextView.setText("TCS Test");
                    holder.mImageView.setImageResource(R.drawable.icons8_sit);
                    break;
                case "4":

                    break;
                default:
                    holder.mTextView.setText("dd");
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(fragment_item_two.this, "Item " + position + " is clicked.", Toast.LENGTH_SHORT).show();
                    switch (position) {
                        case 0:
                            //Intent intent = new Intent();
                            //intent.setClass(getActivity(), DeviceScanActivity.class);
                            //startActivity(intent);
                            //toCalByDay();
                            Intent intent0 = new Intent();
                            intent0.setClass(getActivity(),com.example.android.record.record_by_day.class);
                            startActivity(intent0);

                            break;
                        case 1:
                            Intent intent1 = new Intent();
                            intent1.setClass(getActivity(),com.example.android.record.record_by_week.class);
                            startActivity(intent1);
                            break;
                        default:
                    }
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //Toast.makeText(MainActivity.this, "Item " + position + " is long clicked.", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }

    public void toCalByDay()
    {

        final View item = LayoutInflater.from(getActivity()).inflate(R.layout.custom_dialog,null);//dialog

        new AlertDialog.Builder(getActivity())
                .setTitle("請輸入您的資料")
                .setView(item)
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        EditText editName = (EditText) item.findViewById(R.id.Edit_Name);
                        String name = editName.getText().toString();
                        //Name = name;

                        EditText editAge = (EditText) item.findViewById(R.id.Edit_Age);
                        String age = editAge.getText().toString();
                        //Age = age;

                    }
                })

                .show();

    }
}
