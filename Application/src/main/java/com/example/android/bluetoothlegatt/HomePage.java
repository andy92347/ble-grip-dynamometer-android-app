package com.example.android.bluetoothlegatt;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

public class HomePage extends AppCompatActivity {

    private TextView mTextMessage;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment = null;
            switch (item.getItemId()) {
                //case R.id.navigation_home:
                //    ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("Health");
                //    fragment = new fragment_item_one();
                //    return loadFragment(fragment);
                case R.id.navigation_dashboard:
                    ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("Checkup");
                    fragment = new fragment_item_two();
                    return loadFragment(fragment);
                case R.id.navigation_notifications:
                    ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("Record");
                    fragment = new fragment_item_three();
                    return loadFragment(fragment);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_page);

        loadFragment(new fragment_item_two());

        mTextMessage = (TextView) findViewById(R.id.message);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        /* Action Bar Setting */
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_layout);
        ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("Checkup");

    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }
}
