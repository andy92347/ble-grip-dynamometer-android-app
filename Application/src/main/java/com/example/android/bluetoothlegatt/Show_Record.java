package com.example.android.bluetoothlegatt;

import android.app.Activity;
import android.Manifest;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.SystemClock;
import android.support.v13.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.sql.Date;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import android.view.View.OnClickListener;
import org.apache.commons.math3.stat.regression.SimpleRegression;
/****************************************  line chart  library *********************************************************************/
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis.XAxisPosition;
import com.github.mikephil.charting.components.YAxis.AxisDependency;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.Legend.LegendForm;
import com.github.mikephil.charting.components.Legend.LegendPosition;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.ColorTemplate;
//import com.github.mikephil.charting.utils.ValueFormatter;
import com.github.mikephil.charting.components.MarkerView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.opencsv.CSVWriter;

import java.util.Map;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.android.bluetoothlegatt.LineScope;


public class Show_Record extends Activity {
    private TextView msg;
    private static final String URL_FOR_LOGIN = "http://192.168.43.39/toMySQL/show_record.php";
    public String Namesd;
    public ListView listview;
    public ArrayList<MyJson> listuser;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_record);
        listview = (ListView) findViewById(R.id.listview);
        /************************************************************************************/
        Bundle bundle0311 =this.getIntent().getExtras();
        Namesd = bundle0311.getString("Namesd");
        /************************************************************************************/

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_FOR_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        JsonParser parser = new JsonParser();
                        JsonArray jsonArray = parser.parse(ServerResponse).getAsJsonArray();
                        Gson gson = new Gson();
                        listuser = new ArrayList<>();
                        for (JsonElement MyJson : jsonArray) {
                            MyJson userBean = gson.fromJson(MyJson, MyJson.class);
                            listuser.add(userBean);
                        }

                        listview.setAdapter(new useradapter(Show_Record.this, listuser));
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        // Hiding the progress dialog after all task complete.
                        // progressDialog.dismiss();
                        // Showing error message if something goes wrong.
                        //Toast.makeText(MainActivity.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() {

                // Creating Map String Params.
                Map<String, String> params = new HashMap<String, String>();

                // Adding All values to Params.
                //   DecimalFormat df=new DecimalFormat("#.##");

                //    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss");
                //    String date = df2.format(Calendar.getInstance().getTime());
                /************************************************************************************/
                params.put("Name",Namesd);
                /************************************************************************************/
                // params.put("Duration",Float.toString((float)elapsedTimeMillis/1000));
                // params.put("Max",df.format(max));
                // params.put("Time",date);
                // //params.put("email", EmailHolder);

                return params;
            }

        };

        // Creating RequestQueue.
        RequestQueue requestQueue = Volley.newRequestQueue(Show_Record.this);

        // Adding the StringRequest object into requestQueue.
        requestQueue.add(stringRequest);


    }

    public class MyJson {
        @SerializedName("Name")
        private String myId;
        @SerializedName("Duration")
        private String myDuration;
        @SerializedName("Maximum")
        private String myMax;
        @SerializedName("Time")
        private String myTime;

        public String getMyId() {
            return myId;
        }

        public String getMyDuration() {
            return myDuration;
        }

        public String getMyMax() {
            return myMax;
        }

        public String getMyTime() {
            return myTime;
        }

    }


    public class useradapter extends BaseAdapter {
        public Context con;
        public List<Show_Record.MyJson> list;
        public LayoutInflater inflater;

        public useradapter(Context context, List<Show_Record.MyJson> user) {
            this.con = context;
            this.list = user;
            inflater = LayoutInflater.from(con);

        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = inflater.inflate(R.layout.listview_item, null);
            TextView tv1 = (TextView) view.findViewById(R.id.tv1);
            //TextView tv2 = (TextView) view.findViewById(R.id.tv2);
            //TextView tv3 = (TextView) view.findViewById(R.id.tv3);
            //TextView tv4 = (TextView) view.findViewById(R.id.tv4);
            tv1.setText(list.get(position).getMyId()+"   "+list.get(position).getMyDuration()+"   "+list.get(position).getMyMax()+"   "+list.get(position).getMyTime());
            //tv2.setText(list.get(position).getMyDuration());
            //tv3.setText(list.get(position).getMyMax());
            //tv4.setText(list.get(position).getMyTime());

            tv1.setOnClickListener(new OnClickListener(){

                @Override
                public void onClick(View arg0){

                    try {
                        Toast toast = Toast.makeText(Show_Record.this, "mChart have saved", Toast.LENGTH_SHORT);
                        toast.show();
                        testWriteByStringArray();
                    }
                    catch(Exception e){

                    }
                }
            });
           // tv1.setText("部门:");
            return view;
        }


    }

    public void testWriteByStringArray() throws Exception{
        OutputStreamWriter out = new OutputStreamWriter(new FileOutputStream("/sdcard/test.csv"), Charset.forName("UTF-8"));
        CSVWriter csvWriter = new CSVWriter(out, ',');

        String[] record0 = {"id", "name", "age", "birthday"};
        csvWriter.writeNext(record0);

        String[] record1 = {"1", "张三", "20", "1990-08-08"};
        String[] record2 = {"2", "lisi", "21", "1991-08-08"};
        String[] record3 = {"3", "wangwu", "22", "1992-08-08"};
        String[] record4 = {"3", "wangwu", "22", "1992-08-08"};
        List<String[]> allLines = new ArrayList<String[]>();
        allLines.add(record1);
        allLines.add(record2);
        allLines.add(record3);
        allLines.add(record4);
        csvWriter.writeAll(allLines);
        csvWriter.writeNext(record1);
        csvWriter.writeNext(record0);
        csvWriter.close();

    }
}