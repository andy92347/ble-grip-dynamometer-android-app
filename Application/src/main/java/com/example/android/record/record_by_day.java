package com.example.android.record;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.example.android.bluetoothlegatt.R;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class record_by_day extends AppCompatActivity {

    private LineChart mChart, mChart2, mChart3, mChart4, mChart5;
    private TextView msg;
    private static final String URL_FOR_LOGIN = "http://140.122.103.90:8000/record/byday/gripmeter_duration?name=Abc";
    private static final String URL_FOR_LOGIN_duration = "http://140.122.103.90:8000/record/byday/gripmeter_duration?name=Sam";
    private static final String URL_FOR_LOGIN_max = "http://140.122.103.90:8000/record/byday/gripmeter_max?name=Sam";
    public String Namesd;
    public ArrayList<record_by_day.MyJson> listuser;
    public ArrayList<record_by_day.MyJson> listuser2;
    public ArrayList<record_by_day.MyJson> listuser3;
    public ArrayList<record_by_day.MyJson> listuser4;
    public ArrayList<record_by_day.MyJson> listuser5;
    int i = 0;
    int index = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.record_by_day);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.actionbar_layout);
        ((TextView) getSupportActionBar().getCustomView().findViewById(R.id.tvTitle)).setText("Calculate By Day");

        initmchart();

        requestOne();
        requestTwo();
        //requestThree();
        //requestFour();
        //requestFive();
        //addEntry();
    }


    protected void onResume() {
        super.onResume();

        new Thread(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //requestTwo();
                        //requestThree();
                        //requestFour();
                        //requestFive();
                    }
                });
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
        }).start();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //Toast.makeText(this, "onDestroy", Toast.LENGTH_LONG).show();
    }

    //開始畫圖
    private void addEntry() {
        LineData data = mChart.getData();
        //region AddEnrty
        /*ILineDataSet set = data.getDataSetByIndex(0);
        if (set == null) {
            set = createLineDataSet();
            data.addDataSet(set);
        }


        //data.addXValue(new SimpleDateFormat("mm:ss").format(new Date(System.currentTimeMillis())));
        final ArrayList<String> xLabel = new ArrayList<>();
        xLabel.add("1");
        xLabel.add("2");
        xLabel.add("3");
        xLabel.add("4");
        xLabel.add("5");
        xLabel.add("6");
        mChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabel.get((int)value);
            }
        });
        int val2 = Integer.parseInt("2");

        //Entry entry = new Entry(set.getEntryCount(), (i % 5));
        Entry entry = new Entry(set.getEntryCount(), (i % 5));
        data.addEntry(entry, 0);
        i++;

        mChart.notifyDataSetChanged();
        mChart.setVisibleXRangeMaximum(5000);
        mChart.invalidate();*/
//endregionif
        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            //  ILineDataSet set2 = data.getDataSetByIndex(0);//建立線條2
            if (set == null) {
                set = createSet1();
                // set2 = createSet2();//初始化設定線條2
            }

            //region EntryExample
            /*if (index % 5 == 0) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 1) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 2) {
                data.addEntry(new Entry(set.getEntryCount(), 80), 0);
            } else if (index % 5 == 3) {
                data.addEntry(new Entry(set.getEntryCount(), 20), 0);
            } else if (index % 5 == 4) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            }
            index++;*/


            //data.addEntry(new Entry(0, 0), 0);
            //set.addEntry(new Entry(0, 0));
            //set2.addEntry(new Entry(0, 0));
            //endregion

            //會按照server回傳的數值一個個填入並建立線條
            for (int j = 0; j < 14; j++) {
                //data.addEntry(new Entry(Float.parseFloat(listuser.get(j).myMax), Float.parseFloat(listuser.get(j).myDuration)), 0);
                set.addEntry(new Entry(set.getEntryCount(), Float.parseFloat(listuser.get(j).myDuration)));
                //set2.addEntry(new Entry(set2.getEntryCount(), 4));
                //假如要在線條2加點，就用 set2.addEntry(new Entry(set2.getEntryCount(), X));  ，"X"填入想要加的數字
            }

            final List<String> xlabel = new ArrayList<String>();
            for (int j = 0; j < 7; j++) {
                xlabel.add(listuser.get(j + 7).getMyTime());
            }

            mChart.getXAxis().setValueFormatter(
                    new IAxisValueFormatter() {
                        @Override
                        public String getFormattedValue(float value, AxisBase axis) {
                            if (value < listuser.size() && (int) value == value) {
                                //return xlabel.get((int) value);
                                return listuser.get((int) value).getMyTime();
                                //return String.valueOf(value);
                            } else
                                return "";
                        }
                    }
            );
            data.addDataSet(set);
            //data.addDataSet(set2);
            data.notifyDataChanged();
            mChart.notifyDataSetChanged();
            mChart.setVisibleXRangeMaximum(120);
            mChart.moveViewToX(data.getEntryCount());

        }
        TextView average = (TextView) findViewById(R.id.grip_average_duration);
        TextView deviation = (TextView) findViewById(R.id.grip_deviation_duration);
        ImageView averageImage = (ImageView) findViewById(R.id.grip_average_duration_image);
        ImageView deviationImage = (ImageView) findViewById(R.id.grip_deviation_duration_image);
        math math1 = new math();
        average.setText(String.format("%.2f", math1.getDurationAverage(listuser)[1]));
        deviation.setText(String.format("%.2f", math1.getDurationDeviation(listuser)[1]));
        if(math1.getDurationAverage(listuser)[1]>math1.getDurationAverage(listuser)[0])
            averageImage.setImageResource(R.drawable.icons8_up_arrow_45);
        else
            averageImage.setImageResource(R.drawable.icons8_down_arrow_45);
        if(math1.getDurationDeviation(listuser)[1]>math1.getDurationDeviation(listuser)[0])
            deviationImage.setImageResource(R.drawable.icons8_down_arrow_45);
        else
            deviationImage.setImageResource(R.drawable.icons8_up_arrow_45);
    }

    private void addEntry2() {
        LineData data = mChart2.getData();
        //region AddEnrty
        /*ILineDataSet set = data.getDataSetByIndex(0);
        if (set == null) {
            set = createLineDataSet();
            data.addDataSet(set);
        }


        //data.addXValue(new SimpleDateFormat("mm:ss").format(new Date(System.currentTimeMillis())));
        final ArrayList<String> xLabel = new ArrayList<>();
        xLabel.add("1");
        xLabel.add("2");
        xLabel.add("3");
        xLabel.add("4");
        xLabel.add("5");
        xLabel.add("6");
        mChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabel.get((int)value);
            }
        });
        int val2 = Integer.parseInt("2");

        //Entry entry = new Entry(set.getEntryCount(), (i % 5));
        Entry entry = new Entry(set.getEntryCount(), (i % 5));
        data.addEntry(entry, 0);
        i++;

        mChart.notifyDataSetChanged();
        mChart.setVisibleXRangeMaximum(5000);
        mChart.invalidate();*/
//endregionif

        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            //  ILineDataSet set2 = data.getDataSetByIndex(0);//建立線條2
            if (set == null) {
                set = createSet2();
                // set2 = createSet2();//初始化設定線條2
            }
            //region EntryExample
            /*if (index % 5 == 0) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 1) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 2) {
                data.addEntry(new Entry(set.getEntryCount(), 80), 0);
            } else if (index % 5 == 3) {
                data.addEntry(new Entry(set.getEntryCount(), 20), 0);
            } else if (index % 5 == 4) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            }
            index++;*/


            //data.addEntry(new Entry(0, 0), 0);
            //set.addEntry(new Entry(0, 0));
            //set2.addEntry(new Entry(0, 0));
            //endregion
            //會按照server回傳的數值一個個填入並建立線條
            for (int j = 0; j < 14; j++) {
                //data.addEntry(new Entry(Float.parseFloat(listuser.get(j).myMax), Float.parseFloat(listuser.get(j).myDuration)), 0);
                set.addEntry(new Entry(set.getEntryCount(), Float.parseFloat(listuser2.get(j).myMax)));
                //set2.addEntry(new Entry(set2.getEntryCount(), 4));
                //假如要在線條2加點，就用 set2.addEntry(new Entry(set2.getEntryCount(), X));  ，"X"填入想要加的數字
            }

            final List<String> xlabel = new ArrayList<String>();
            for (int j = 0; j < listuser2.size(); j++) {
                xlabel.add(listuser2.get(j).myMax);
            }

            mChart2.getXAxis().setValueFormatter(
                    new IAxisValueFormatter() {
                        @Override
                        public String getFormattedValue(float value, AxisBase axis) {
                            if (value < listuser2.size() && (int) value == value) {
                                //return xlabel.get((int) value);
                                return listuser2.get((int) value).getMyTime();
                            } else
                                return "";
                        }
                    }
            );
            data.addDataSet(set);
            //data.addDataSet(set2);
            data.notifyDataChanged();
            mChart2.notifyDataSetChanged();
            mChart2.setVisibleXRangeMaximum(120);
            mChart2.moveViewToX(data.getEntryCount());

        }
        TextView average = (TextView) findViewById(R.id.grip_average_max);
        TextView deviation = (TextView) findViewById(R.id.grip_deviation_max);
        ImageView averageImage = (ImageView) findViewById(R.id.grip_average_max_image);
        ImageView deviationImage = (ImageView) findViewById(R.id.grip_deviation_max_image);
        math math2 = new math();
        average.setText(String.format("%.2f", math2.getMaxAverage(listuser2)[1]));
        deviation.setText(String.format("%.2f", math2.getMaxDeviation(listuser2)[1]));
        if(math2.getMaxAverage(listuser2)[1]>math2.getMaxAverage(listuser2)[0])
            averageImage.setImageResource(R.drawable.icons8_up_arrow_45);
        else
            averageImage.setImageResource(R.drawable.icons8_down_arrow_45);
        if(math2.getMaxDeviation(listuser2)[1]>math2.getMaxDeviation(listuser2)[0])
            deviationImage.setImageResource(R.drawable.icons8_down_arrow_45);
        else
            deviationImage.setImageResource(R.drawable.icons8_up_arrow_45);
    }

    private void addEntry3() {
        LineData data = mChart3.getData();
        //region AddEnrty
        /*ILineDataSet set = data.getDataSetByIndex(0);
        if (set == null) {
            set = createLineDataSet();
            data.addDataSet(set);
        }


        //data.addXValue(new SimpleDateFormat("mm:ss").format(new Date(System.currentTimeMillis())));
        final ArrayList<String> xLabel = new ArrayList<>();
        xLabel.add("1");
        xLabel.add("2");
        xLabel.add("3");
        xLabel.add("4");
        xLabel.add("5");
        xLabel.add("6");
        mChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabel.get((int)value);
            }
        });
        int val2 = Integer.parseInt("2");

        //Entry entry = new Entry(set.getEntryCount(), (i % 5));
        Entry entry = new Entry(set.getEntryCount(), (i % 5));
        data.addEntry(entry, 0);
        i++;

        mChart.notifyDataSetChanged();
        mChart.setVisibleXRangeMaximum(5000);
        mChart.invalidate();*/
//endregionif
        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            //  ILineDataSet set2 = data.getDataSetByIndex(0);//建立線條2
            if (set == null) {
                set = createSet3();
                // set2 = createSet2();//初始化設定線條2
            }

            //region EntryExample
            /*if (index % 5 == 0) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 1) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 2) {
                data.addEntry(new Entry(set.getEntryCount(), 80), 0);
            } else if (index % 5 == 3) {
                data.addEntry(new Entry(set.getEntryCount(), 20), 0);
            } else if (index % 5 == 4) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            }
            index++;*/


            //data.addEntry(new Entry(0, 0), 0);
            //set.addEntry(new Entry(0, 0));
            //set2.addEntry(new Entry(0, 0));
            //endregion

            //會按照server回傳的數值一個個填入並建立線條
            for (int j = 0; j < listuser3.size(); j++) {
                //data.addEntry(new Entry(Float.parseFloat(listuser.get(j).myMax), Float.parseFloat(listuser.get(j).myDuration)), 0);
                set.addEntry(new Entry(set.getEntryCount(), Float.parseFloat(listuser3.get(j).myDuration)));
                //set2.addEntry(new Entry(set2.getEntryCount(), 4));
                //假如要在線條2加點，就用 set2.addEntry(new Entry(set2.getEntryCount(), X));  ，"X"填入想要加的數字
            }

            final List<String> xlabel = new ArrayList<String>();
            for (int j = 0; j < listuser3.size(); j++) {
                xlabel.add(listuser3.get(j).myMax);
            }

            mChart3.getXAxis().setValueFormatter(
                    new IAxisValueFormatter() {
                        @Override
                        public String getFormattedValue(float value, AxisBase axis) {
                            if (value < listuser3.size() && (int) value == value) {
                                //return xlabel.get((int) value);
                                return listuser3.get((int) value).getMyTime();
                            } else
                                return "";
                        }
                    }
            );
            data.addDataSet(set);
            //data.addDataSet(set2);
            data.notifyDataChanged();
            mChart3.notifyDataSetChanged();
            mChart3.setVisibleXRangeMaximum(120);
            mChart3.moveViewToX(data.getEntryCount());

        }

    }

    private void addEntry4() {
        LineData data = mChart4.getData();
        //region AddEnrty
        /*ILineDataSet set = data.getDataSetByIndex(0);
        if (set == null) {
            set = createLineDataSet();
            data.addDataSet(set);
        }


        //data.addXValue(new SimpleDateFormat("mm:ss").format(new Date(System.currentTimeMillis())));
        final ArrayList<String> xLabel = new ArrayList<>();
        xLabel.add("1");
        xLabel.add("2");
        xLabel.add("3");
        xLabel.add("4");
        xLabel.add("5");
        xLabel.add("6");
        mChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabel.get((int)value);
            }
        });
        int val2 = Integer.parseInt("2");

        //Entry entry = new Entry(set.getEntryCount(), (i % 5));
        Entry entry = new Entry(set.getEntryCount(), (i % 5));
        data.addEntry(entry, 0);
        i++;

        mChart.notifyDataSetChanged();
        mChart.setVisibleXRangeMaximum(5000);
        mChart.invalidate();*/
//endregionif
        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            //  ILineDataSet set2 = data.getDataSetByIndex(0);//建立線條2
            if (set == null) {
                set = createSet4();
                // set2 = createSet2();//初始化設定線條2
            }

            //region EntryExample
            /*if (index % 5 == 0) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 1) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 2) {
                data.addEntry(new Entry(set.getEntryCount(), 80), 0);
            } else if (index % 5 == 3) {
                data.addEntry(new Entry(set.getEntryCount(), 20), 0);
            } else if (index % 5 == 4) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            }
            index++;*/


            //data.addEntry(new Entry(0, 0), 0);
            //set.addEntry(new Entry(0, 0));
            //set2.addEntry(new Entry(0, 0));
            //endregion

            //會按照server回傳的數值一個個填入並建立線條
            for (int j = 0; j < listuser4.size(); j++) {
                //data.addEntry(new Entry(Float.parseFloat(listuser.get(j).myMax), Float.parseFloat(listuser.get(j).myDuration)), 0);
                set.addEntry(new Entry(set.getEntryCount(), Float.parseFloat(listuser4.get(j).myDuration)));
                //set2.addEntry(new Entry(set2.getEntryCount(), 4));
                //假如要在線條2加點，就用 set2.addEntry(new Entry(set2.getEntryCount(), X));  ，"X"填入想要加的數字
            }

            final List<String> xlabel = new ArrayList<String>();
            for (int j = 0; j < listuser4.size(); j++) {
                xlabel.add(listuser4.get(j).myMax);
            }

            mChart4.getXAxis().setValueFormatter(
                    new IAxisValueFormatter() {
                        @Override
                        public String getFormattedValue(float value, AxisBase axis) {
                            if (value < listuser4.size() && (int) value == value) {
                                //return xlabel.get((int) value);
                                return listuser4.get((int) value).getMyTime();
                            } else
                                return "";
                        }
                    }
            );
            data.addDataSet(set);
            //data.addDataSet(set2);
            data.notifyDataChanged();
            mChart4.notifyDataSetChanged();
            mChart4.setVisibleXRangeMaximum(120);
            mChart4.moveViewToX(data.getEntryCount());

        }

    }

    private void addEntry5() {
        LineData data = mChart5.getData();
        //region AddEnrty
        /*ILineDataSet set = data.getDataSetByIndex(0);
        if (set == null) {
            set = createLineDataSet();
            data.addDataSet(set);
        }


        //data.addXValue(new SimpleDateFormat("mm:ss").format(new Date(System.currentTimeMillis())));
        final ArrayList<String> xLabel = new ArrayList<>();
        xLabel.add("1");
        xLabel.add("2");
        xLabel.add("3");
        xLabel.add("4");
        xLabel.add("5");
        xLabel.add("6");
        mChart.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                return xLabel.get((int)value);
            }
        });
        int val2 = Integer.parseInt("2");

        //Entry entry = new Entry(set.getEntryCount(), (i % 5));
        Entry entry = new Entry(set.getEntryCount(), (i % 5));
        data.addEntry(entry, 0);
        i++;

        mChart.notifyDataSetChanged();
        mChart.setVisibleXRangeMaximum(5000);
        mChart.invalidate();*/
//endregionif
        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            //  ILineDataSet set2 = data.getDataSetByIndex(0);//建立線條2
            if (set == null) {
                set = createSet5();
                // set2 = createSet2();//初始化設定線條2
            }

            //region EntryExample
            /*if (index % 5 == 0) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 1) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            } else if (index % 5 == 2) {
                data.addEntry(new Entry(set.getEntryCount(), 80), 0);
            } else if (index % 5 == 3) {
                data.addEntry(new Entry(set.getEntryCount(), 20), 0);
            } else if (index % 5 == 4) {
                data.addEntry(new Entry(set.getEntryCount(), 50), 0);
            }
            index++;*/


            //data.addEntry(new Entry(0, 0), 0);
            //set.addEntry(new Entry(0, 0));
            //set2.addEntry(new Entry(0, 0));
            //endregion

            //會按照server回傳的數值一個個填入並建立線條
            for (int j = 0; j < listuser5.size(); j++) {
                //data.addEntry(new Entry(Float.parseFloat(listuser.get(j).myMax), Float.parseFloat(listuser.get(j).myDuration)), 0);
                set.addEntry(new Entry(set.getEntryCount(), Float.parseFloat(listuser5.get(j).myDuration)));
                //set2.addEntry(new Entry(set2.getEntryCount(), 4));
                //假如要在線條2加點，就用 set2.addEntry(new Entry(set2.getEntryCount(), X));  ，"X"填入想要加的數字
            }

            final List<String> xlabel = new ArrayList<String>();
            for (int j = 0; j < listuser5.size(); j++) {
                xlabel.add(listuser5.get(j).myMax);
            }

            mChart5.getXAxis().setValueFormatter(
                    new IAxisValueFormatter() {
                        @Override
                        public String getFormattedValue(float value, AxisBase axis) {
                            if (value < listuser5.size() && (int) value == value) {
                                //return xlabel.get((int) value);
                                return listuser5.get((int) value).getMyTime();
                            } else
                                return "";
                        }
                    }
            );
            data.addDataSet(set);
            //data.addDataSet(set2);
            data.notifyDataChanged();
            mChart5.notifyDataSetChanged();
            mChart5.setVisibleXRangeMaximum(120);
            mChart5.moveViewToX(data.getEntryCount());

        }

    }

    //線條1初始化設定
    private LineDataSet createSet1() {

        LineDataSet set = new LineDataSet(null, "Gripping Duration");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(Color.parseColor("#4fc3f7"));
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(3f);
        set.setDrawCircles(true);
        set.setCircleColor(Color.parseColor("#4fc3f7"));
        set.setDrawCircleHole(false);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        set.setDrawFilled(true);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.chart_filled_color);
        set.setFillDrawable(drawable);
        return set;
    }

    private LineDataSet createSet2() {

        LineDataSet set = new LineDataSet(null, "Gripping Maximum");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(Color.parseColor("#00e676"));
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(3f);
        set.setDrawCircles(true);
        set.setCircleColor(Color.parseColor("#00e676"));
        set.setDrawCircleHole(false);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        set.setDrawFilled(true);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.chart_filled_color2);

        set.setFillDrawable(drawable);
        return set;
    }

    private LineDataSet createSet3() {

        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(Color.parseColor("#ffca28"));
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(3f);
        set.setDrawCircles(true);
        set.setCircleColor(Color.parseColor("#ffca28"));
        set.setDrawCircleHole(false);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        set.setDrawFilled(true);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.chart_filled_color3);

        set.setFillDrawable(drawable);
        return set;
    }

    private LineDataSet createSet4() {

        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(Color.parseColor("#5e35b1"));
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(3f);
        set.setDrawCircles(true);
        set.setCircleColor(Color.parseColor("#5e35b1"));
        set.setDrawCircleHole(false);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        set.setDrawFilled(true);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.chart_filled_color4);

        set.setFillDrawable(drawable);
        return set;
    }

    private LineDataSet createSet5() {

        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColor(Color.parseColor("#e91e63"));
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(3f);
        set.setDrawCircles(true);
        set.setCircleColor(Color.parseColor("#e91e63"));
        set.setDrawCircleHole(false);
        set.setFillAlpha(65);
        set.setFillColor(ColorTemplate.getHoloBlue());
        set.setHighLightColor(Color.rgb(244, 117, 117));
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(9f);
        set.setDrawValues(false);
        set.setDrawFilled(true);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.chart_filled_color5);

        set.setFillDrawable(drawable);
        return set;
    }

    private void initmchart() {
        //region Chart1
        mChart = (LineChart) findViewById(R.id.record_chart1);
        mChart.setDescription(null);
        mChart.setNoDataText("暂时尚无数据");
        mChart.setTouchEnabled(true);
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);

        mChart.setPinchZoom(true);
        //mChart.setBackgroundColor(Color.WHITE);

        LineData data = new LineData();
        data.setValueTextColor(Color.BLACK);
        mChart.setData(data);
        Legend l = mChart.getLegend();
        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.BLACK);

        XAxis xl = mChart.getXAxis();
        xl.setTextColor(Color.parseColor("#9e9e9e"));
        xl.setDrawGridLines(false);
        xl.setAvoidFirstLastClipping(true);
        //xl.setTypeface(Typeface.createFromFile(getResources(), "fonts/calibri_title.ttf"));
        //xl.setSpaceBetweenLabels(3);
        xl.setEnabled(true);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTextColor(Color.parseColor("#9e9e9e"));
        //leftAxis.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/calibri_title.ttf"));
        //leftAxis.setAxisMaxValue(5.00f);
        //leftAxis.setAxisMinValue(0.00f);
        //leftAxis.setStartAtZero(false);
        leftAxis.setDrawGridLines(false);
        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);

        leftAxis.resetAxisMinimum();
        leftAxis.resetAxisMaximum();
        //endregion

        //region Chart2
        mChart2 = (LineChart) findViewById(R.id.record_chart2);
        mChart2.setDescription(null);
        mChart2.setNoDataText("暂时尚无数据");
        mChart2.setTouchEnabled(true);
        mChart2.setDragEnabled(true);
        mChart2.setScaleEnabled(true);
        mChart2.setDrawGridBackground(false);

        mChart2.setPinchZoom(true);
        //mChart.setBackgroundColor(Color.WHITE);

        LineData data2 = new LineData();
        data2.setValueTextColor(Color.BLACK);
        mChart2.setData(data2);
        Legend l2 = mChart2.getLegend();
        l2.setForm(Legend.LegendForm.LINE);
        l2.setTextColor(Color.BLACK);

        XAxis xl2 = mChart2.getXAxis();
        xl2.setTextColor(Color.parseColor("#9e9e9e"));
        xl2.setDrawGridLines(false);
        xl2.setAvoidFirstLastClipping(true);
        //xl.setTypeface(Typeface.createFromFile(getResources(), "fonts/calibri_title.ttf"));
        //xl.setSpaceBetweenLabels(3);
        xl2.setEnabled(true);
        xl2.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis2 = mChart2.getAxisLeft();
        leftAxis2.setTextColor(Color.parseColor("#9e9e9e"));
        //leftAxis.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/calibri_title.ttf"));
        //leftAxis.setAxisMaxValue(5.00f);
        //leftAxis.setAxisMinValue(0.00f);
        //leftAxis.setStartAtZero(false);
        leftAxis2.setDrawGridLines(false);
        YAxis rightAxis2 = mChart2.getAxisRight();
        rightAxis2.setEnabled(false);

        leftAxis2.resetAxisMinimum();
        leftAxis2.resetAxisMaximum();
        //endregion

        //region Other Chart
         /*
        //region Chart3
        mChart3 = (LineChart) findViewById(R.id.record_chart3);
        mChart3.setDescription(null);
        mChart3.setNoDataText("暂时尚无数据");
        mChart3.setTouchEnabled(true);
        mChart3.setDragEnabled(true);
        mChart3.setScaleEnabled(true);
        mChart3.setDrawGridBackground(false);

        mChart3.setPinchZoom(true);
        //mChart.setBackgroundColor(Color.WHITE);

        LineData data3 = new LineData();
        data3.setValueTextColor(Color.BLACK);
        mChart3.setData(data3);
        Legend l3 = mChart2.getLegend();
        l3.setForm(Legend.LegendForm.LINE);
        l3.setTextColor(Color.BLACK);

        XAxis xl3 = mChart3.getXAxis();
        xl3.setTextColor(Color.parseColor("#9e9e9e"));
        xl3.setDrawGridLines(false);
        xl3.setAvoidFirstLastClipping(true);
        //xl.setTypeface(Typeface.createFromFile(getResources(), "fonts/calibri_title.ttf"));
        //xl.setSpaceBetweenLabels(3);
        xl3.setEnabled(true);
        xl3.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis3 = mChart3.getAxisLeft();
        leftAxis3.setTextColor(Color.parseColor("#9e9e9e"));
        //leftAxis.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/calibri_title.ttf"));
        //leftAxis.setAxisMaxValue(5.00f);
        //leftAxis.setAxisMinValue(0.00f);
        //leftAxis.setStartAtZero(false);
        leftAxis3.setDrawGridLines(false);
        YAxis rightAxis3 = mChart3.getAxisRight();
        rightAxis3.setEnabled(false);

        leftAxis3.resetAxisMinimum();
        leftAxis3.resetAxisMaximum();
        //endregion

        //region Chart4
        mChart4 = (LineChart) findViewById(R.id.record_chart4);
        mChart4.setDescription(null);
        mChart4.setNoDataText("暂时尚无数据");
        mChart4.setTouchEnabled(true);
        mChart4.setDragEnabled(true);
        mChart4.setScaleEnabled(true);
        mChart4.setDrawGridBackground(false);

        mChart4.setPinchZoom(true);
        //mChart.setBackgroundColor(Color.WHITE);

        LineData data4 = new LineData();
        data4.setValueTextColor(Color.BLACK);
        mChart4.setData(data4);
        Legend l4 = mChart4.getLegend();
        l4.setForm(Legend.LegendForm.LINE);
        l4.setTextColor(Color.BLACK);

        XAxis xl4 = mChart4.getXAxis();
        xl4.setTextColor(Color.parseColor("#9e9e9e"));
        xl4.setDrawGridLines(false);
        xl4.setAvoidFirstLastClipping(true);
        //xl.setTypeface(Typeface.createFromFile(getResources(), "fonts/calibri_title.ttf"));
        //xl.setSpaceBetweenLabels(3);
        xl4.setEnabled(true);
        xl4.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis4 = mChart4.getAxisLeft();
        leftAxis4.setTextColor(Color.parseColor("#9e9e9e"));
        //leftAxis.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/calibri_title.ttf"));
        //leftAxis.setAxisMaxValue(5.00f);
        //leftAxis.setAxisMinValue(0.00f);
        //leftAxis.setStartAtZero(false);
        leftAxis4.setDrawGridLines(false);
        YAxis rightAxis4 = mChart4.getAxisRight();
        rightAxis4.setEnabled(false);

        leftAxis4.resetAxisMinimum();
        leftAxis4.resetAxisMaximum();
        //endregion

        //region Chart5
        mChart5 = (LineChart) findViewById(R.id.record_chart5);
        mChart5.setDescription(null);
        mChart5.setNoDataText("暂时尚无数据");
        mChart5.setTouchEnabled(true);
        mChart5.setDragEnabled(true);
        mChart5.setScaleEnabled(true);
        mChart5.setDrawGridBackground(false);

        mChart5.setPinchZoom(true);
        //mChart.setBackgroundColor(Color.WHITE);

        LineData data5 = new LineData();
        data5.setValueTextColor(Color.BLACK);
        mChart5.setData(data5);
        Legend l5 = mChart5.getLegend();
        l5.setForm(Legend.LegendForm.LINE);
        l5.setTextColor(Color.BLACK);

        XAxis xl5 = mChart5.getXAxis();
        xl5.setTextColor(Color.parseColor("#9e9e9e"));
        xl5.setDrawGridLines(false);
        xl5.setAvoidFirstLastClipping(true);
        //xl.setTypeface(Typeface.createFromFile(getResources(), "fonts/calibri_title.ttf"));
        //xl.setSpaceBetweenLabels(3);
        xl5.setEnabled(true);
        xl5.setPosition(XAxis.XAxisPosition.BOTTOM);

        YAxis leftAxis5 = mChart5.getAxisLeft();
        leftAxis5.setTextColor(Color.parseColor("#9e9e9e"));
        //leftAxis.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/calibri_title.ttf"));
        //leftAxis.setAxisMaxValue(5.00f);
        //leftAxis.setAxisMinValue(0.00f);
        //leftAxis.setStartAtZero(false);
        leftAxis5.setDrawGridLines(false);
        YAxis rightAxis5 = mChart5.getAxisRight();
        rightAxis5.setEnabled(false);

        leftAxis5.resetAxisMinimum();
        leftAxis5.resetAxisMaximum();
        //endregion
        */
         //endregion
    }

    public class MyJson {
        @SerializedName("name")
        private String myId;
        @SerializedName("duration")
        private String myDuration;
        @SerializedName("max")
        private String myMax;
        @SerializedName("datetime")
        private String myTime;

        public String getMyId() {
            return myId;
        }

        public String getMyDuration() {
            return myDuration;
        }

        public String getMyMax() {
            return myMax;
        }

        public String getMyTime() {
            return myTime.substring(5, 10);
        }

    }

    public void requestOne() {
        StringRequest stringRequest = new StringRequest(URL_FOR_LOGIN_duration,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        JsonParser parser = new JsonParser();
                        JsonArray jsonArray = parser.parse(ServerResponse).getAsJsonArray();
                        Gson gson = new Gson();
                        listuser = new ArrayList<>();
                        for (JsonElement MyJson : jsonArray) {
                            record_by_day.MyJson userBean = gson.fromJson(MyJson, record_by_day.MyJson.class);
                            listuser.add(userBean);
                        }
                        //Toast.makeText(record_by_day.this, "Connected", Toast.LENGTH_LONG).show();
                        addEntry();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(record_by_day.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(record_by_day.this);
        requestQueue.add(stringRequest);
    }

    public void requestTwo() {
        StringRequest stringRequest = new StringRequest(URL_FOR_LOGIN_max,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        JsonParser parser = new JsonParser();
                        JsonArray jsonArray = parser.parse(ServerResponse).getAsJsonArray();
                        Gson gson = new Gson();
                        listuser2 = new ArrayList<>();
                        for (JsonElement MyJson : jsonArray) {
                            record_by_day.MyJson userBean = gson.fromJson(MyJson, record_by_day.MyJson.class);
                            listuser2.add(userBean);
                        }
                        //Toast.makeText(record_by_day.this, "2Connected", Toast.LENGTH_LONG).show();
                        addEntry2();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(record_by_day.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(record_by_day.this);
        requestQueue.add(stringRequest);
    }

    public void requestThree() {
        StringRequest stringRequest = new StringRequest(URL_FOR_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        JsonParser parser = new JsonParser();
                        JsonArray jsonArray = parser.parse(ServerResponse).getAsJsonArray();
                        Gson gson = new Gson();
                        listuser3 = new ArrayList<>();
                        for (JsonElement MyJson : jsonArray) {
                            record_by_day.MyJson userBean = gson.fromJson(MyJson, record_by_day.MyJson.class);
                            listuser3.add(userBean);
                        }
                        //Toast.makeText(record_by_day.this, "3Connected", Toast.LENGTH_LONG).show();
                        addEntry3();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(record_by_day.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(record_by_day.this);
        requestQueue.add(stringRequest);
    }

    public void requestFour() {
        StringRequest stringRequest = new StringRequest(URL_FOR_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        JsonParser parser = new JsonParser();
                        JsonArray jsonArray = parser.parse(ServerResponse).getAsJsonArray();
                        Gson gson = new Gson();
                        listuser4 = new ArrayList<>();
                        for (JsonElement MyJson : jsonArray) {
                            record_by_day.MyJson userBean = gson.fromJson(MyJson, record_by_day.MyJson.class);
                            listuser4.add(userBean);
                        }
                        //Toast.makeText(record_by_day.this, "4Connected", Toast.LENGTH_LONG).show();
                        addEntry4();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(record_by_day.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(record_by_day.this);
        requestQueue.add(stringRequest);
    }

    public void requestFive() {
        StringRequest stringRequest = new StringRequest(URL_FOR_LOGIN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String ServerResponse) {
                        JsonParser parser = new JsonParser();
                        JsonArray jsonArray = parser.parse(ServerResponse).getAsJsonArray();
                        Gson gson = new Gson();
                        listuser5 = new ArrayList<>();
                        for (JsonElement MyJson : jsonArray) {
                            record_by_day.MyJson userBean = gson.fromJson(MyJson, record_by_day.MyJson.class);
                            listuser5.add(userBean);
                        }
                        //Toast.makeText(record_by_day.this, "5Connected", Toast.LENGTH_LONG).show();
                        addEntry5();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        Toast.makeText(record_by_day.this, volleyError.toString(), Toast.LENGTH_LONG).show();
                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(record_by_day.this);
        requestQueue.add(stringRequest);
    }

    public class math {
        private float average[] = {0, 0};
        private float deviation[] = {0, 0};

        public float[] getDurationAverage(ArrayList<record_by_day.MyJson> ServerResponse) {
            float sum[] = {0, 0};
            //private float average;
            if (ServerResponse.size() != 14) {
                Toast.makeText(record_by_day.this, "Number Error", Toast.LENGTH_LONG).show();
                return average;
            } else {
                for (int i = (ServerResponse.size() / 2); i < ServerResponse.size(); i++) {
                    sum[1] = sum[1] + Float.parseFloat(ServerResponse.get(i).myDuration);
                }
                average[1] = sum[1] / (ServerResponse.size() / 2);

                for (int i = 0; i < (ServerResponse.size() / 2); i++) {
                    sum[0] = sum[0] + Float.parseFloat(ServerResponse.get(i).myDuration);
                }
                average[0] = sum[0] / (ServerResponse.size() / 2);
                return average;
            }

        }

        public float[] getDurationDeviation(ArrayList<record_by_day.MyJson> ServerResponse) {
            float sum[] = {0, 0};
            if (ServerResponse.size() != 14) {
                Toast.makeText(record_by_day.this, "Number Error", Toast.LENGTH_LONG).show();
                return deviation;
            }
            else {
                for (int i = (ServerResponse.size() / 2); i <ServerResponse.size(); i++) {
                    sum[1] = sum[1] + (float) Math.pow((double) (Float.parseFloat(ServerResponse.get(i).myDuration) - average[1]), 2);
                }
                deviation[1] = (float) Math.sqrt((double) (sum[1] / (ServerResponse.size() / 2)));

                for (int i = 0; i < (ServerResponse.size() / 2); i++) {
                    sum[0] = sum[0] + (float) Math.pow((double) (Float.parseFloat(ServerResponse.get(i).myDuration) - average[0]), 2);
                }
                deviation[0] = (float) Math.sqrt((double) (sum[0] / (ServerResponse.size() / 2)));

                return deviation;
            }
        }
        public float[] getMaxAverage(ArrayList<record_by_day.MyJson> ServerResponse) {
            float sum[] = {0, 0};
            //private float average;
            if (ServerResponse.size() != 14) {
                Toast.makeText(record_by_day.this, "Number Error", Toast.LENGTH_LONG).show();
                return average;
            } else {
                for (int i = (ServerResponse.size() / 2); i < ServerResponse.size(); i++) {
                    sum[1] = sum[1] + Float.parseFloat(ServerResponse.get(i).myMax);
                }
                average[1] = sum[1] / (ServerResponse.size() / 2);

                for (int i = 0; i < (ServerResponse.size() / 2); i++) {
                    sum[0] = sum[0] + Float.parseFloat(ServerResponse.get(i).myMax);
                }
                average[0] = sum[0] / (ServerResponse.size() / 2);
                return average;
            }

        }
        public float[] getMaxDeviation(ArrayList<record_by_day.MyJson> ServerResponse) {
            float sum[] = {0, 0};
            if (ServerResponse.size() != 14) {
                Toast.makeText(record_by_day.this, "Number Error", Toast.LENGTH_LONG).show();
                return deviation;
            }
            else {
                for (int i = (ServerResponse.size() / 2); i <ServerResponse.size(); i++) {
                    sum[1] = sum[1] + (float) Math.pow((double) (Float.parseFloat(ServerResponse.get(i).myMax) - average[1]), 2);
                }
                deviation[1] = (float) Math.sqrt((double) (sum[1] / (ServerResponse.size() / 2)));

                for (int i = 0; i < (ServerResponse.size() / 2); i++) {
                    sum[0] = sum[0] + (float) Math.pow((double) (Float.parseFloat(ServerResponse.get(i).myMax) - average[0]), 2);
                }
                deviation[0] = (float) Math.sqrt((double) (sum[0] / (ServerResponse.size() / 2)));

                return deviation;
            }
        }
    }

    public List<Entry> DatatoArray(List<record_by_day.MyJson> listuser) {
        List<record_by_day.MyJson> li = listuser;
        List<Entry> entry = new ArrayList<>();

        for (i = 0; i < listuser.size(); i++) {
            entry.add(new Entry(Float.parseFloat(li.get(i).myMax), Float.parseFloat(li.get(i).myDuration)));
        }
        i = 1;
        return entry;
    }

    private LineDataSet createLineDataSet() {
        LineDataSet set = new LineDataSet(null, "");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);

        // 折线的颜色
        //set.setColor(ColorTemplate.getHoloBlue());
        //set.setColor(Color.parseColor("#5c6bc0"));
        set.setColor(Color.parseColor("#4fc3f7"));
        set.setCircleColor(Color.parseColor("#4fc3f7"));
        set.setDrawFilled(true);
        set.setCubicIntensity(0.2f);
        set.setLineWidth(3f);
        set.setCircleRadius(5f);
        set.setFillAlpha(128);
        //set.setFillColor(ColorTemplate.getHoloBlue());
        //set.setFillColor(Color.parseColor("#ffeb3b"));
        // set.setFillColor(getResources().getColor(R.drawable.chart_filled_color,null));
        set.setHighLightColor(Color.GREEN);
        set.setDrawValues(false);

        //LineRadarDataSet setFillDrawable(Drawable);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.chart_filled_color);
        set.setFillDrawable(drawable);
        // set.setValueTextColor(Color.WHITE);
        // set.setValueTextSize(10f);
        // set.setDrawValues(true);
        return set;
    }


}
