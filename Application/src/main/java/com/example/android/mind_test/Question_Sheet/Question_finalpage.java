package com.example.android.mind_test.Question_Sheet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;


public class Question_finalpage extends AppCompatActivity {

    private TextView score_TextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_question_finalpage);

        score_TextView=(TextView) findViewById(R.id.score_TextView);

        Bundle bundle=getIntent().getExtras();
         int score=bundle.getInt("scoreFinal");
         String show_score=String.valueOf(score);

         score_TextView.setText(show_score+"分");


    }
    //設定傳出分數給資料庫處
    public void End(View view)
    {
        //score為總分

    }
}
