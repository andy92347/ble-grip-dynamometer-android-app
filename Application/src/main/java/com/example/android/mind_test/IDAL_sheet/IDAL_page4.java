package com.example.android.mind_test.IDAL_sheet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;

public class IDAL_page4 extends AppCompatActivity {

    public static final String Q3_ANSWER_KEY = "Q4";

    //設定參數,radio=5
    private TextView m_tv_no;
    private TextView m_tv_question;
    private RadioButton m_radio_a;
    private RadioButton m_radio_b;
    private RadioButton m_radio_c;
    private RadioButton m_radio_d;
    private RadioButton m_radio_e;



    public String lastscore,score;
    public String ScoreValue;
    public boolean StopAlarm=false;

    private CharSequence m_answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idal_page4_layout);

        //get last score value
        Bundle bundle=getIntent().getExtras();
        lastscore=bundle.getString("score3");


        //hint text
        ScoreValue=lastscore;
        Toast toast = Toast.makeText(this, ScoreValue, Toast.LENGTH_SHORT);
        toast.show();

        init();
    }
    //返回時score重新計算
    @Override
    protected  void onResume()
    {
        super.onResume();
        score=lastscore;
    }


    //起始畫面，設定起始畫面的按鈕,radio=5
    private void init() {
        m_tv_no = (TextView)findViewById(R.id.idal_tv_no);
        m_tv_question = (TextView)findViewById(R.id.idal_question_tv);
        m_radio_a = (RadioButton)findViewById(R.id.idal_radio_A_05);
        m_radio_b = (RadioButton)findViewById(R.id.idal_radio_B_05);
        m_radio_c = (RadioButton)findViewById(R.id.idal_radio_C_05);
        m_radio_d = (RadioButton)findViewById(R.id.idal_radio_D_05);
        m_radio_e = (RadioButton)findViewById(R.id.idal_radio_E_05);





//題號
        m_tv_no.setText("4.");
//第一題的問題
        m_tv_question.setText(Html.fromHtml(getString(R.string.idal_04)));
//radio第一個選擇
        m_radio_a.setText(Html.fromHtml(getString(R.string.idal_ans_4A)));
//radio第二個選擇
        m_radio_b.setText(Html.fromHtml(getString(R.string.idal_ans_4B)));
        m_radio_c.setText(Html.fromHtml(getString(R.string.idal_ans_4C)));
        m_radio_d.setText(Html.fromHtml(getString(R.string.idal_ans_4D)));
        m_radio_e.setText(Html.fromHtml(getString(R.string.idal_ans_4E)));

    }

    // 按下 MAIN
    public void next(View view) {
        Intent intent = new Intent(this,IDAL_page5.class);
        getScore();
        if(StopAlarm)
        {
            this.onResume();
        }
        else {




            //score_part02
            Bundle bundle = new Bundle();
            bundle.putString("score4", score);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    // 按下 BACK
    public void back(View view) {
        finish();
    }

    //radio=5
    public void getScore()
    {

        if(m_radio_a.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_b.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_c.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_d.isChecked())
        {
            score+="4.家務維持"+"\n";
            StopAlarm=false;
        }
        else if(m_radio_e.isChecked())
        {
            score+="4.家務維持"+"\n";
            StopAlarm=false;
        }

        else
        {
            //hint text
            StopAlarm=true;
            String alarm="請選擇一個選項喔!";
            Toast toast = Toast.makeText(this, alarm, Toast.LENGTH_SHORT);
            toast.show();

        }

    }


    //radio_on_click,radio=5
    public void radio_a_onclick05(View view)
    {
        m_radio_a.isChecked();
    }
    public void radio_b_onclick05(View view)
    {
        m_radio_b.isChecked();
    }
    public void radio_c_onclick05(View view)
    {
        m_radio_c.isChecked();
    }
    public void radio_d_onclick05(View view)
    {
        m_radio_d.isChecked();
    }
    public void radio_e_onclick05(View view)
    {
        m_radio_e.isChecked();
    }

    //radio_on_click

}
