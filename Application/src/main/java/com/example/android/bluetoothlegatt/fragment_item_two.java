package com.example.android.bluetoothlegatt;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class fragment_item_two extends Fragment {
    private MyAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //just change the fragment_dashboard
        //with the fragment you want to inflate
        //like if the class is HomeFragment it should have R.layout.home_fragment
        //if it is DashboardFragment it should have R.layout.fragment_dashboard
        View view = inflater.inflate(R.layout.fragment_item_two, null);

        /*android.widget.Button swToBluetooth = (Button) view.findViewById(R.id.button4);

        swToBluetooth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), DeviceScanActivity.class);
                startActivity(intent);
            }
        });*/

        ArrayList<String> myDataset = new ArrayList<>();
        /* set how many list */
        for (int i = 0; i < 3; i++) {
            myDataset.add(Integer.toString(i));
        }
        mAdapter = new MyAdapter(myDataset);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);

        return view;
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private List<String> mData;

        /* different view(text,image) is set in here */
        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTextView;
            public ImageView mImageView;

            public ViewHolder(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.info_text);
                mImageView = (ImageView) v.findViewById(R.id.info_img);
            }
        }

        @Override
        public int getItemViewType(int position) {
            return position;
        }

        public MyAdapter(List<String> data) {
            mData = data;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.fragment_item_two_list, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            /* different view(text,image) is set in here */
            switch (mData.get(position)) {
                case "0":
                    holder.mTextView.setText("Grip Meter");
                    break;
                case "1":
                    holder.mTextView.setText("Mind Test");
                    holder.mImageView.setImageResource(R.drawable.icons8_idea2);
                    //holder.itemView.setBackgroundResource(R.drawable.buttonshape1);
                    //holder.itemView.setBackgroundColor(getResources().getColor(R.color.holo_light_primary,null));
                    break;
                case "2":
                    holder.mTextView.setText("TCS Test");
                    holder.mImageView.setImageResource(R.drawable.icons8_sit);
                    break;
                case "4":
                    //ViewGroup.MarginLayoutParams marginLayoutParams =
                    //        (ViewGroup.MarginLayoutParams) mRecyclerView.getLayoutParams();
                    //marginLayoutParams.setMargins(10, 10, 10, 10);
                    //holder.itemView.setLayoutParams(marginLayoutParams);
                    break;
                default:
                    holder.mTextView.setText("dd");
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(fragment_item_two.this, "Item " + position + " is clicked.", Toast.LENGTH_SHORT).show();
                    switch (position) {
                        case 0:
                            Intent intent0 = new Intent();
                            intent0.setClass(getActivity(), DeviceScanActivity.class);
                            startActivity(intent0);
                            break;
                        case 1:
                            Intent intent1 = new Intent();
                            intent1.setClass(getActivity(), com.example.android.mind_test.Mind_test.class);
                            startActivity(intent1);
                            break;
                        case 2:
                            Intent intent2 = new Intent();
                            intent2.setClass(getActivity(), com.example.android.timer.timer.class);
                            startActivity(intent2);
                            break;
                        default:
                    }
                }
            });

            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //Toast.makeText(MainActivity.this, "Item " + position + " is long clicked.", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }

}
