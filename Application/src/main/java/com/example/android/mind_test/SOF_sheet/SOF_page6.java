package com.example.android.mind_test.SOF_sheet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;
import com.example.android.mind_test.Mind_test;

public class SOF_page6 extends AppCompatActivity {

    public static final String Q3_ANSWER_KEY = "Q4";

    private TextView m_tv_no;
    private TextView m_tv_question;
    private RadioButton m_radio_a;
    private RadioButton m_radio_b;



    public int lastscore,score,score_part01,score_part02;
    public String ScoreValue;
    public boolean StopAlarm=false;

    private CharSequence m_answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sof_page6_layout);

        //get last score value
        Bundle bundle=getIntent().getExtras();
        lastscore=bundle.getInt("score5");
        score_part01=bundle.getInt("score_part01");
        score_part02=bundle.getInt("score_part02");

        //hint text
        ScoreValue=String.valueOf(lastscore);
        Toast toast = Toast.makeText(this, ScoreValue, Toast.LENGTH_SHORT);
        toast.show();

        init();
    }
    //返回時score重新計算
    @Override
    protected  void onResume()
    {
        super.onPause();
        score=lastscore;
    }


    private void init() {
        m_tv_no = (TextView)findViewById(R.id.sof_tv_no);
        m_tv_question = (TextView)findViewById(R.id.sof_question_tv);
        m_radio_a = (RadioButton)findViewById(R.id.sof_radio_yes);
        m_radio_b = (RadioButton)findViewById(R.id.sof_radio_no);


        m_tv_no.setText("6:憂鬱?");
        m_tv_question.setText(Html.fromHtml(getString(R.string.SOF_06)));
        //radio第一個選擇
        m_radio_a.setText(Html.fromHtml(getString(R.string.SOF_Yes)));
        //radio第二個選擇
        m_radio_b.setText(Html.fromHtml(getString(R.string.SOF_No)));


    }

    // 按下 MAIN
    public void next(View view) {
        Intent intent = new Intent(this,SOF_finalpage.class);
        getScore();
        if(StopAlarm)
        {
            this.onResume();
        }
        else {
            Bundle bundle = new Bundle();
            bundle.putInt("score_part03", score);
            bundle.putInt("score_part01", score_part01);
            bundle.putInt("score_part02", score_part02);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    // 按下 BACK
    public void back(View view) {
        finish();
    }

    public void getScore()
    {

        if(m_radio_a.isChecked())
        {
            score+=1;
            StopAlarm=false;
        }
        else if(m_radio_b.isChecked())
        {
            score+=0;
            StopAlarm=false;
        }

        else
        {
            //hint text
            StopAlarm=true;
            String alarm="請選擇一個選項喔!";
            Toast toast = Toast.makeText(this, alarm, Toast.LENGTH_SHORT);
            toast.show();

        }

    }


    //radio_on_click
    public void radio_a_onclick (View view)
    {
        m_radio_a.isChecked();
    }

    public void radio_b_onclick (View view)
    {
        m_radio_b.isChecked();
    }


    //radio_on_click

}
