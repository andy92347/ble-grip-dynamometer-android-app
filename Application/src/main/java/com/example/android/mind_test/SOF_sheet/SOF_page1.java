package com.example.android.mind_test.SOF_sheet;



import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;

public class SOF_page1 extends AppCompatActivity {

    public static final String Q1_ANSWER_KEY = "Q1";


    //設定參數
    private TextView m_tv_no;
    private TextView m_tv_question;
    private RadioButton m_radio_a;
    private RadioButton m_radio_b;


    public int score;
    public boolean StopAlarm=false;


    private CharSequence m_answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sof_page1_layout);
        init();
    }

    //返回時score重新計算
    @Override
    protected  void onResume()
    {
        super.onResume();
        score=0;
    }

    //起始畫面，設定起始畫面的按鈕
    private void init() {
        m_tv_no = (TextView)findViewById(R.id.sof_tv_no);
        m_tv_question = (TextView)findViewById(R.id.sof_question_tv);
        m_radio_a = (RadioButton)findViewById(R.id.sof_radio_yes);
        m_radio_b = (RadioButton)findViewById(R.id.sof_radio_no);





//題號
        m_tv_no.setText("1:體重減輕?");
//第一題的問題
        m_tv_question.setText(Html.fromHtml(getString(R.string.SOF_01)));
//radio第一個選擇
        m_radio_a.setText(Html.fromHtml(getString(R.string.SOF_Yes)));
//radio第二個選擇
        m_radio_b.setText(Html.fromHtml(getString(R.string.SOF_No)));

    }
    //返回上一頁
    public void back(View view) {
        finish();
    }
    // 按下 NEXT
    public void next(View view) {


        // 建立新 Intent: new Intent( 來源 , 目的);
        getScore();
        if(StopAlarm)
        {
            this.onResume();
        }
        else {
            Intent intent = new Intent(this,SOF_page2.class);
            Bundle bundle = new Bundle();
            bundle.putInt("score1", score);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }


    public void getScore()
    {

        if(m_radio_a.isChecked())
        {
            score+=1;
            StopAlarm=false;
        }
        else if(m_radio_b.isChecked())
        {
            score+=0;
            StopAlarm=false;
        }

        else
        {
            //hint text
            StopAlarm=true;
            String alarm="請選擇一個選項喔!";
            Toast toast = Toast.makeText(this, alarm, Toast.LENGTH_SHORT);
            toast.show();

        }

    }



    public void radio_a_onclick(View view)
    {
        m_radio_a.isChecked();
    }

    public void radio_b_onclick(View view)
    {
        m_radio_b.isChecked();
    }




}