package com.example.android.mind_test.SOF_sheet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;


public class SOF_finalpage extends AppCompatActivity {

    private TextView score_01_TextView,score_02_TextView,score_03_TextView;
    int score_part01,score_part02,score_part03;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sof_finalpage);

        score_01_TextView=(TextView) findViewById(R.id.score_01_TextView);
        score_02_TextView=(TextView) findViewById(R.id.score_02_TextView);
        score_03_TextView=(TextView) findViewById(R.id.score_03_TextView);

        Bundle bundle=getIntent().getExtras();
         score_part01=bundle.getInt("score_part01");
         score_part02=bundle.getInt("score_part02");
         score_part03=bundle.getInt("score_part03");
         String show_01=String.valueOf(score_part01);
        String show_02=String.valueOf(score_part02);
        String show_03=String.valueOf(score_part03);

         score_01_TextView.setText("您的第一部份得分為(衰弱):"+show_01+"分");
        score_02_TextView.setText("您的第二部份得分為(跌倒):"+show_02+"分");
        score_03_TextView.setText("您的第三部份得分為(憂鬱):"+show_03+"分");


    }
    //設定傳出分數給資料庫處
    public void End(View view)
    {
        //score為總分

    }
}
