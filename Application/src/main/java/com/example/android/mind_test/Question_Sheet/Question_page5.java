package com.example.android.mind_test.Question_Sheet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;

public class Question_page5 extends AppCompatActivity {

    public static final String Q3_ANSWER_KEY = "Q4";

    private TextView m_tv_no;
    private TextView m_tv_question;
    private RadioButton m_radio_a;
    private RadioButton m_radio_b;
    private RadioButton m_radio_c;
    private RadioButton m_radio_d;
    private RadioButton m_radio_e;


    public int lastscore,score;
    public String ScoreValue;
    public boolean StopAlarm=false;

    private CharSequence m_answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.question_page5_layout);

        //get last score value
        Bundle bundle=getIntent().getExtras();
        lastscore=bundle.getInt("score4");

        //hint text
        ScoreValue=String.valueOf(lastscore);
        Toast toast = Toast.makeText(this, ScoreValue, Toast.LENGTH_SHORT);
        toast.show();

        init();
    }
    //返回時score重新計算
    @Override
    protected  void onResume()
    {
        super.onPause();
        score=lastscore;
    }


    private void init() {
        m_tv_no = (TextView) findViewById(R.id.tv_no);
        m_tv_question = (TextView) findViewById(R.id.tv_question);
        m_radio_a = (RadioButton) findViewById(R.id.radio_a);
        m_radio_b = (RadioButton) findViewById(R.id.radio_b);
        m_radio_c = (RadioButton) findViewById(R.id.radio_c);
        m_radio_d=(RadioButton)findViewById(R.id.radio_d);
        m_radio_e=(RadioButton)findViewById(R.id.radio_e);

        m_tv_no.setText("5");
        m_tv_question.setText(Html.fromHtml(getString(R.string.question_5)));
        //radio第一個選擇
        m_radio_a.setText(Html.fromHtml(getString(R.string.question_1_radio_a)));
        //radio第二個選擇
        m_radio_b.setText(Html.fromHtml(getString(R.string.question_1_radio_b)));
        //radio第三個選擇
        m_radio_c.setText(Html.fromHtml(getString(R.string.question_1_radio_c)));
        //radio第四個選擇
        m_radio_d.setText(Html.fromHtml(getString(R.string.question_1_radio_d)));
        //radio第五個選擇
        m_radio_e.setText(Html.fromHtml(getString(R.string.question_1_radio_e)));

    }

    // 按下 MAIN
    public void next(View view) {
        Intent intent = new Intent(this,Question_page6.class);
        getScore();
        if(StopAlarm)
        {
            this.onResume();
        }
        else {
            Bundle bundle = new Bundle();
            bundle.putInt("score5", score);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    // 按下 BACK
    public void back(View view) {
        finish();
    }

    public void getScore()
    {

        if(m_radio_a.isChecked())
        {
            score+=0;
            StopAlarm=false;
        }
        else if(m_radio_b.isChecked())
        {
            score+=1;
            StopAlarm=false;
        }
        else if(m_radio_c.isChecked())
        {
            score+=2;
            StopAlarm=false;
        }
        else if(m_radio_d.isChecked())
        {
            score+=3;
            StopAlarm=false;
        }
        else if(m_radio_e.isChecked())
        {
            score+=4;
            StopAlarm=false;
        }
        else
        {
            //hint text
            StopAlarm=true;
            String alarm="請選擇一個選項喔!";
            Toast toast = Toast.makeText(this, alarm, Toast.LENGTH_SHORT);
            toast.show();

        }

    }


    //radio_on_click
    public void radio_a_onclick (View view)
    {
        m_radio_a.isChecked();
    }

    public void radio_b_onclick (View view)
    {
        m_radio_b.isChecked();
    }

    public void radio_c_onclick (View view)
    {
        m_radio_c.isChecked();
    }
    public void radio_d_onclick (View view)
    {
        m_radio_d.isChecked();
    }
    public void radio_e_onclick (View view)
    {
        m_radio_e.isChecked();
    }
    //radio_on_click

}
