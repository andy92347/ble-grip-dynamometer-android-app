package com.example.android.mind_test;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.android.bluetoothlegatt.R;

import java.util.ArrayList;
import java.util.List;

public class Mind_test extends AppCompatActivity {
    private MyAdapter mAdapter;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mind_test);
        ArrayList<String> myDataset = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            myDataset.add(Integer.toString(i));
        }
        mAdapter = new MyAdapter(myDataset);
        mRecyclerView = (RecyclerView) findViewById(R.id.list_view);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setAdapter(mAdapter);
    }

    public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
        private List<String> mData;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView mTextView;

            public ViewHolder(View v) {
                super(v);
                mTextView = (TextView) v.findViewById(R.id.info_text);
            }
        }

        public MyAdapter(List<String> data) {
            mData = data;
        }

        @Override
        public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.mind_test_list, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            //holder.mTextView.setText(mData.get(position));
            switch (mData.get(position)) {
                case "0":
                    holder.mTextView.setText("簡式健康量表(BSRS)");
                    break;
                case "1":
                    holder.mTextView.setText("SOF衰弱評估");
                    break;
                case "2":
                    holder.mTextView.setText("IDAL工具性日常活動評估");
                    break;
            }

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (position) {
                        case 0:
                            Intent intent0 = new Intent();
                            intent0.setClass(Mind_test.this, com.example.android.mind_test.Question_Sheet.Question_main.class);
                            startActivity(intent0);

                            break;
                        case 1:
                            Intent intent1 = new Intent();
                            intent1.setClass(Mind_test.this, com.example.android.mind_test.SOF_sheet.SOF_main.class);
                            startActivity(intent1);

                            break;
                        case 2:
                            Intent intent2 = new Intent();
                            intent2.setClass(Mind_test.this, com.example.android.mind_test.IDAL_sheet.IDAL_main.class);
                            startActivity(intent2);

                            break;
                        default:
                    }
                }
            });
            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    //Toast.makeText(MainActivity.this, "Item " + position + " is long clicked.", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }
}
