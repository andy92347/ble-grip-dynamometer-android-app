package com.example.android.mind_test.IDAL_sheet;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;
import com.example.android.mind_test.Mind_test;

public class IDAL_page8 extends AppCompatActivity {

    public static final String Q3_ANSWER_KEY = "Q4";

    //設定參數,radio=3
    private TextView m_tv_no;
    private TextView m_tv_question;
    private RadioButton m_radio_a;
    private RadioButton m_radio_b;
    private RadioButton m_radio_c;



    public String lastscore,score;
    public String ScoreValue;
    public boolean StopAlarm=false;

    private CharSequence m_answer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idal_page8_layout);

        //get last score value
        Bundle bundle=getIntent().getExtras();
        lastscore=bundle.getString("score7");

        //hint text
        ScoreValue=lastscore;
        Toast toast = Toast.makeText(this, ScoreValue, Toast.LENGTH_SHORT);
        toast.show();

        init();
    }
    //返回時score重新計算
    @Override
    protected  void onResume()
    {
        super.onPause();
        score=lastscore;
    }


    //起始畫面，設定起始畫面的按鈕,radio=3
    private void init() {
        m_tv_no = (TextView)findViewById(R.id.idal_tv_no);
        m_tv_question = (TextView)findViewById(R.id.idal_question_tv);
        m_radio_a = (RadioButton)findViewById(R.id.idal_radio_A_03);
        m_radio_b = (RadioButton)findViewById(R.id.idal_radio_B_03);
        m_radio_c = (RadioButton)findViewById(R.id.idal_radio_C_03);






//題號
        m_tv_no.setText("8.");
//第一題的問題
        m_tv_question.setText(Html.fromHtml(getString(R.string.idal_08)));
//radio第一個選擇
        m_radio_a.setText(Html.fromHtml(getString(R.string.idal_ans_8A)));
//radio第二個選擇
        m_radio_b.setText(Html.fromHtml(getString(R.string.idal_ans_8B)));
        m_radio_c.setText(Html.fromHtml(getString(R.string.idal_ans_8C)));

    }

    // 按下 MAIN
    public void next(View view) {
        Intent intent = new Intent(this,IDAL_finalpage.class);
        getScore();
        if(StopAlarm)
        {
            this.onResume();
        }
        else {
            Bundle bundle = new Bundle();
            bundle.putString("score8",score);

            intent.putExtras(bundle);
            startActivity(intent);
        }
    }

    // 按下 BACK
    public void back(View view) {
        finish();
    }

    //radio=3
    public void getScore()
    {

        if(m_radio_a.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_b.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_c.isChecked())
        {
            score+="8.處理財務的能力";
            StopAlarm=false;
        }

        else
        {
            //hint text
            StopAlarm=true;
            String alarm="請選擇一個選項喔!";
            Toast toast = Toast.makeText(this, alarm, Toast.LENGTH_SHORT);
            toast.show();

        }

    }


    //radio=3
    public void radio_a_onclick03(View view)
    {
        m_radio_a.isChecked();
    }

    public void radio_b_onclick03(View view)
    {
        m_radio_b.isChecked();
    }

    public void radio_c_onclick03(View view)
    {
        m_radio_c.isChecked();
    }

    //radio_on_click

}
