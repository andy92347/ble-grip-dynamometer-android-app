package com.example.android.mind_test.IDAL_sheet;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.bluetoothlegatt.R;

@SuppressLint("Registered")
public class IDAL_page2 extends AppCompatActivity {

    public static final String Q2_ANSWER_KEY = "Q2";

    //設定參數,radio=5
    private TextView m_tv_no;
    private TextView m_tv_question;
    private RadioButton m_radio_a;
    private RadioButton m_radio_b;
    private RadioButton m_radio_c;
    private RadioButton m_radio_d;
    private RadioButton m_radio_e;


    public String lastscore,score;
    public String ScoreValue;
    public boolean StopAlarm=false;

    private CharSequence m_answer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.idal_page2_layout);

        //get last score value
        Bundle bundle=getIntent().getExtras();
        lastscore=bundle.getString("score1");

        //hint text
        ScoreValue=lastscore;
        Toast toast = Toast.makeText(this, ScoreValue, Toast.LENGTH_SHORT);
        toast.show();

        init();

    }

    //返回時score重新計算
    @Override
    protected  void onResume()
    {
        super.onResume();
        score=lastscore;
    }


    //起始畫面，設定起始畫面的按鈕,radio=5
    private void init() {
        m_tv_no = (TextView)findViewById(R.id.idal_tv_no);
        m_tv_question = (TextView)findViewById(R.id.idal_question_tv);
        m_radio_a = (RadioButton)findViewById(R.id.idal_radio_A_05);
        m_radio_b = (RadioButton)findViewById(R.id.idal_radio_B_05);
        m_radio_c = (RadioButton)findViewById(R.id.idal_radio_C_05);
        m_radio_d = (RadioButton)findViewById(R.id.idal_radio_D_05);
        m_radio_e = (RadioButton)findViewById(R.id.idal_radio_E_05);





//題號
        m_tv_no.setText("2.");
//第一題的問題
        m_tv_question.setText(Html.fromHtml(getString(R.string.idal_02)));
//radio第一個選擇
        m_radio_a.setText(Html.fromHtml(getString(R.string.idal_ans_2A)));
//radio第二個選擇
        m_radio_b.setText(Html.fromHtml(getString(R.string.idal_ans_2B)));
        m_radio_c.setText(Html.fromHtml(getString(R.string.idal_ans_2C)));
        m_radio_d.setText(Html.fromHtml(getString(R.string.idal_ans_2D)));
        m_radio_e.setText(Html.fromHtml(getString(R.string.idal_ans_2E)));



    }



    // 按下 BACK
    public void back(View view) {
        finish();
    }

    // 按下 NEXT
    public void next(View view) {
        // 建立新 Intent: new Intent( 來源 , 目的)
        Intent intent = new Intent(this, IDAL_page3.class);
        // 建立新 Intent: new Intent( 來源 , 目的);
        getScore();
        if(StopAlarm)
        {
            this.onResume();
        }
        else {
            Bundle bundle = new Bundle();
            bundle.putString("score2", score);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }


    //radio=5
    public void getScore()
    {

        if(m_radio_a.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_b.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_c.isChecked())
        {
            score+="";
            StopAlarm=false;
        }
        else if(m_radio_d.isChecked())
        {
            score+="2.外出活動"+"\n";
            StopAlarm=false;
        }
        else if(m_radio_e.isChecked())
        {
            score+="2.外出活動"+"\n";
            StopAlarm=false;
        }

        else
        {
            //hint text
            StopAlarm=true;
            String alarm="請選擇一個選項喔!";
            Toast toast = Toast.makeText(this, alarm, Toast.LENGTH_SHORT);
            toast.show();

        }

    }

    //radio_on_click,radio=5
        public void radio_a_onclick05(View view)
        {
            m_radio_a.isChecked();
        }
        public void radio_b_onclick05(View view)
        {
            m_radio_b.isChecked();
        }
        public void radio_c_onclick05(View view)
    {
        m_radio_c.isChecked();
    }
        public void radio_d_onclick05(View view)
    {
        m_radio_d.isChecked();
    }
        public void radio_e_onclick05(View view)
    {
        m_radio_e.isChecked();
    }




    //radio_on_click

}