package com.example.android.mind_test.SOF_sheet;



import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.android.bluetoothlegatt.R;
import com.example.android.mind_test.SOF_sheet.SOF_page1;

public class SOF_main extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sof_main_layout);

    }




    // 按下 NEXT (切下一個畫面 Activit1)
    public void next(View view) {
        //Intent(現在這個位置  ,  選擇下一頁的位置);
        Intent intent = new Intent(this, SOF_page1.class);

        startActivity(intent);

    }
}